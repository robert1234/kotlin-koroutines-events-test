package com.example.kotlinandeventtestapp.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Contacts
import android.util.Log
import android.widget.TextView
import com.example.kotlinandeventtestapp.Event.TimerEvent
import com.example.kotlinandeventtestapp.Model.EventBus
import com.example.kotlinandeventtestapp.Model.TimerModel
import com.example.kotlinandeventtestapp.R
import com.example.kotlinandeventtestapp.Thread.TimerThread
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.consumeEach
import java.util.*

@ExperimentalCoroutinesApi
@FlowPreview
@ObsoleteCoroutinesApi
class MainActivity : AppCompatActivity() {
    private var timeTextView: TextView? = null
    private var timerThread: TimerThread? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initButtons()
        runTimerThread()
        subscribeEvents()
    }

    private fun initButtons() {
        timeTextView = findViewById(R.id.time_text_view);
    }

    private fun subscribeEvents() {
        GlobalScope.launch() {
            EventBus.on(this) { event ->
                if (event is TimerEvent) {
                    Log.d("MainActivity", event.toString())
                    updateTimeTextView(event)
                }
            }
        }
    }

    private fun runTimerThread() {
        timerThread = TimerThread();
        val timer = Timer()

        timeTextView?.text = timerThread!!.timerModel.value.toString()
        timer.scheduleAtFixedRate(timerThread, 3000L, 1000L)
    }

    private fun updateTimeTextView(event: TimerEvent) {
        this@MainActivity.runOnUiThread(java.lang.Runnable {
            val timerModel: TimerModel = event.timerModel
            timeTextView?.text = timerModel.value.toString()
        })
    }
}
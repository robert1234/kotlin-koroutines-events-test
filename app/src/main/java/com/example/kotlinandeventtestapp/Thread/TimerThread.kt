package com.example.kotlinandeventtestapp.Thread

import android.util.Log
import com.example.kotlinandeventtestapp.Event.TimerEvent
import com.example.kotlinandeventtestapp.Model.EventBus
import com.example.kotlinandeventtestapp.Model.TimerModel
import com.example.kotlinandeventtestapp.Model.TimerModelMode
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

@ExperimentalCoroutinesApi
class TimerThread : TimerTask() {
    val timerModel: TimerModel = TimerModel.create()
    private val timerModelMode: TimerModelMode = TimerModelMode.COUNT_UP

    override fun run() {
        Log.d("TimerThread", "Run on: " + this.timerModel.value.toString())
        this.nextTick()
    }

    private fun nextTick() {
        when(timerModelMode) {
            TimerModelMode.COUNT_UP -> {
                timerModel.increment()
            }
            TimerModelMode.COUNT_DOWN -> {
                timerModel.decrement()
            }
            else -> {}
        }
        GlobalScope.launch{ emitChange() }
    }

    private suspend fun emitChange() {
        Log.d("TimerThread", this.timerModel.value.toString())
        EventBus.dispatch(TimerEvent(this.timerModel));
    }
}
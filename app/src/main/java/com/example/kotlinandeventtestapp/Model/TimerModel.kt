package com.example.kotlinandeventtestapp.Model

class TimerModel {
    var value: Int = 0
    var minRange: Int = 0
    var maxRange: Int = 1000000

    fun value(): Int {
        return this.value
    }

    fun increment(): Int {
        this.value += 1

        if (this.value > maxRange) {
            this.value = minRange
        }

        return this.value
    }

    fun decrement(): Int {
        this.value -= 1

        if (this.value < minRange) {
            this.value = maxRange
        }

        return this.value
    }

    companion object {
        fun create(minRange: Int = 0, maxRange: Int = 1000000): TimerModel {
            val timerModel = TimerModel()
            timerModel.minRange = minRange
            timerModel.maxRange = maxRange
            return timerModel
        }
    }
}
package com.example.kotlinandeventtestapp.Model

enum class TimerModelMode(value: Int) {
    COUNT_UP(2),
    COUNT_DOWN(0),
    PAUSE(1)
}
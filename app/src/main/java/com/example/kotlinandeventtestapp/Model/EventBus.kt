package com.example.kotlinandeventtestapp.Model

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.flow.*

object EventBus {
    private val events = MutableSharedFlow<Any>()

    suspend fun dispatch(event: Any) {
        events.emit(event)
    }

    suspend fun on(coroutineScope: CoroutineScope, handler: suspend (Any) -> Unit) =
        coroutineScope.launch(start = CoroutineStart.UNDISPATCHED) {
            events.asSharedFlow().collect { event -> handler(event) }
        }
}
